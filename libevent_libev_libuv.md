
[libhv](https://github.com/ithewei/libhv) Like libevent, libev, and libuv, libhv provides event-loop with non-blocking IO and timer, but simpler apis and richer protocols.


[网络库libevent、libev、libuv对比](https://blog.csdn.net/lijinqi1987/article/details/71214974)

Libevent、libev、libuv三个网络库，都是c语言实现的异步事件库_Asynchronousevent library）_。

异步事件库本质上是提供异步事件通知（Asynchronous Event Notification，AEN）的。异步事件通知机制就是根据发生的事件，调用相应的回调函数进行处理。

**事件（Event）：** 事件是异步事件通知机制的核心，比如fd事件、超时事件、信号事件、定时器事件。有时候也称事件为事件处理器（EventHandler），这个名称更形象，因为Handler本身表示了包含处理所需数据（或数据的地址）和处理的方法（回调函数），更像是面向对象思想中的称谓。

**事件循环（EventLoop）：** 等待并分发事件。事件循环用于管理事件。

对于应用程序来说，这些只是异步事件库提供的API，封装了异步事件库跟操作系统的交互，异步事件库会选择一种操作系统提供的机制来实现某一种事件，比如利用Unix/Linux平台的epoll机制实现网络IO事件，在同时存在多种机制可以利用时，异步事件库会采用最优机制。

对比下三个库：
=======

libevent :名气最大，应用最广泛，历史悠久的跨平台事件库；

libev :较libevent而言，设计更简练，性能更好，但对Windows支持不够好；

libuv :开发node的过程中需要一个跨平台的事件库，他们首选了libev，但又要支持Windows，故重新封装了一套，linux下用libev实现，Windows下用IOCP实现；


在github上的影响力：
=============

![](images/20170505163959722.png)
![](images/20170505164016535.png)  

可见，目前libuv的影响力最大，其次是libevent，libev关注的人较少。

  

优先级、事件循环、线程安全维度的对比
==================

<table><tr><td valign="top">
<p align="left"><strong>&nbsp; <span style="font-family:'Microsoft YaHei';font-size:14px;">
&nbsp;特性</strong></p>
</td>
<td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>libevent</strong></p>
</td>
<td valign="top">
<p align="left"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">&nbsp;libev</strong></p>
</td>
<td valign="top">
<p align="left"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">&nbsp;libuv</strong></p>
</td>
</tr><tr><td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;"><strong>&nbsp;&nbsp;</strong></p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;"><strong>优先级</strong></p>
</td>
<td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">激活的事件组织在优先级队列中，<span style="font-family:'Microsoft YaHei';font-size:14px;">各<span style="font-family:'Microsoft YaHei';font-size:14px;">类事</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">件<span style="font-family:'Microsoft YaHei';font-size:14px;">默认的<span style="font-family:'Microsoft YaHei';font-size:14px;">优<span style="font-family:'Microsoft YaHei';font-size:14px;">先<span style="font-family:'Microsoft YaHei';font-size:14px;">级<span style="font-family:'Microsoft YaHei';font-size:14px;">是相同的，可以<span style="font-family:'Microsoft YaHei';font-size:14px;">通过设置</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">事件的优先级使其优先被处理</p>
</td>
<td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">也是通过优先级队列来管理激活的<span style="font-family:'Microsoft YaHei';font-size:14px;">时间，</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">也可以<span style="font-family:'Microsoft YaHei';font-size:14px;">设置事件优先级</p>
</td>
<td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">没有优先级概念，按照固定的顺序访</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">问各类事件</p>
</td>
</tr><tr><td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;"><strong><br></strong></p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;"><strong>事件循环 &nbsp; &nbsp;</strong></p>
</td>
<td valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">&nbsp;</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">&nbsp; &nbsp; event_base用于管理事件</p>
</td>
<td width="277" colspan="2" valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">激活的事件组织在优先级队列中，各类事件默认的优先级是相<span style="font-family:'Microsoft YaHei';font-size:14px;">同的，</p>
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">可以通 &nbsp;过设置事件的优先级 &nbsp; 使其优先被处理</p>
</td>
</tr><tr><td valign="top">
<p align="left"><span style="font-weight:bold;"><span style="font-family:'Microsoft YaHei';font-size:14px;"><br></p>
<p align="left"><span style="font-weight:bold;"><span style="font-family:'Microsoft YaHei';font-size:14px;">线程安全</p>
</td>
<td width="415" colspan="3" valign="top">
<p align="left"><span style="font-family:'Microsoft YaHei';font-size:14px;">event_base和loop都不是线程安全的，一个event_base或loop实例只能在用户的一个线程内访问<span style="font-family:'Microsoft YaHei';font-size:14px;">（一般是主线程），注册到event_base或者loop的event都是串行访问的，即每个执行过程中，会<span style="font-family:'Microsoft YaHei';font-size:14px;">按照优先级顺序访问已经激活的事件，执行其回调函数。所以在仅使用一个event_base或loop的情<span style="font-family:'Microsoft YaHei';font-size:14px;">况下，回调函数的执行不存在并行关系</p>
<p align="left"><br></p>
</td>
</tr></table>

  

线程安全

event_base和loop都不是线程安全的，一个event_base或loop实例只能在用户的一个线程内访问（一般是主线程），

注册到event_base或者loop的event都是串行访问的，即每个执行过程中，会按照优先级顺序访问已经激活的事件，执行其回调函数。

所以在仅使用一个event_base或loop的情况下，回调函数的执行不存在并行关系
  

事件种类
====

<table><tr><td nowrap="nowrap" valign="top">
<p align="center"><strong>type</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libevent</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libev</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libuv</strong></p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>IO</strong></p>
</td>
<td valign="top">
<p align="center">fd</p>
</td>
<td valign="top">
<p align="center">io</p>
</td>
<td valign="top">
<p align="center">fs_event</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">计时器（mono clock）</strong></p>
</td>
<td valign="top">
<p align="center">timer</p>
</td>
<td valign="top">
<p align="center">timer</p>
</td>
<td valign="top">
<p align="center">timter</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">计时器（wall clock）</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">periodic</p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>信号</strong></p>
</td>
<td valign="top">
<p align="center">signal</p>
</td>
<td valign="top">
<p align="center">signal</p>
</td>
<td valign="top">
<p align="center">signal</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>进程控制</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">child</p>
</td>
<td valign="top">
<p align="center">process</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">文件stat</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">stat</p>
</td>
<td valign="top">
<p align="center">fs_poll</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">每次循环都会执行的Idle事件</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">idle</p>
</td>
<td valign="top">
<p align="center">idle</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">循环block之前执行</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">prepare</p>
</td>
<td valign="top">
<p align="center">prepare</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">循环blcck之后执行</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">check</p>
</td>
<td valign="top">
<p align="center">check</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">嵌套loop</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">embed</p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>fork</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">fork</p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">loop销毁之前的清理工作</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">cleanup</p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong><span style="font-family:'Microsoft YaHei';font-size:14px;">操作另一个线程中的loop</strong></p>
</td>
<td valign="top">
<p align="center">--</p>
</td>
<td valign="top">
<p align="center">async</p>
</td>
<td valign="top">
<p align="center">async</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>stream ( tcp, pipe, tty )</strong></p>
</td>
<td valign="top">
<p align="center">stream ( tcp, pipe, tty )</p>
</td>
<td valign="top">
<p align="center">stream ( tcp, pipe, tty )</p>
</td>
<td valign="top">
<p align="center">stream ( tcp, pipe, tty )</p>
</td>
</tr></table>
  
这个对比对于libev和libuv更有意义，对于libevent，很多都是跟其设计思想有关的。 libev中的embed很少用，libuv没有也没关系；cleanup完全可以用libuv中的async_exit来替代；libuv没有fork事件。  

  

可移植性
====

三个库都支持Linux, *BSD, Mac OS X, Solaris, Windows  

<table><tr><td nowrap="nowrap" valign="top">
<p align="center"><strong>type</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libevent</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libev</strong></p>
</td>
<td nowrap="nowrap" valign="top">
<p align="center"><strong>libuv</strong></p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>dev/poll (Solaris)</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>event ports</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>kqueue (*BSD)</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>POSIX select</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>Windows select</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>Windows IOCP</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">N</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>poll</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr><tr><td valign="top">
<p align="center"><strong>epoll</strong></p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
<td valign="top">
<p align="center">y</p>
</td>
</tr></table>
  

对于Unix/Linux平台，没有什么大不同，优先选择epoll，对于windows，libevent、libev都使用select检测和分发事件（不I/O），libuv在windows下使用IOCP。libevent有一个socket handle, 在windows上使用IOCP进行读写。libev没有类似的。但是libevent的IOCP支持也不是很好（性能不高）。所以如果是在windows平台下，使用原生的IOCP进行I/O，或者使用libuv。

  

异步架构程序设计原则
----------

1、回调函数不可以执行过长时间，因为一个loop中可能包含其他事件，尤其是会影响一些准确度要求比较高的timer。

2、尽量采用库中所缓存的时间，有时候需要根据时间差来执行timeout之类的操作。当然能够利用库中的timer最好。

参考：http://zheolong.github.io/blog/libevent-libev-libuv/

    https://cpp.libhunt.com/