
[Libevent:event](https://www.jianshu.com/p/a60e9d53b0cb)

Libevent 是一个快速的可移植非阻塞的IO库，它的设计目标是：

可移植性，快速，可伸缩，方便

Libevent由一下几个部分组成：

## evutil:

用以解决不同平台上网络实现的不同

## event,event_base:

Libevent的核心,提供统一的接口给不同的平台。可以对IO事件,定时事件,系统信号进行响应。

## bufferevent:

对Libevent的核心event_base进行更方便的包装。支持缓冲的读写，以便进行更快速的非阻塞IO操作。

## evbuffer:

提供了buffervents下的buffer,提供了函数进行更方便高效的调用

## evhttp:

http组件

## evdns：

dns组件

## evrpc:

rpc组件