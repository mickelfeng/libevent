
![](images/20150203180814584.png)事件驱动（event-driven），高性能;

轻量级，专注于网络，不如ACE那么臃肿庞大； 

源代码相当精炼、易读； 

跨平台，支持Windows、Linux、*BSD和Mac Os； 

支持多种I/O多路复用技术， epoll、poll、dev/poll、select和kqueue等； 

支持I/O，定时器和信号等事件； 

注册事件优先级；

一个很好的分析libevent工作流程的地址：http://blog.csdn.net/luotuo44/article/details/38501341

测试程序实例：

libevent开发流程：

    event_base_new ->event_set->event_base_set->event_add->event_base_dispatch

```
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <event.h>
#include<stdlib.h>
#include<string.h>
 
#define PORT 9999
#define BACKLOG 5
#define MEM_SIZE 1024
 
struct event_base* base;
 
struct sock_ev {
	struct event* read_ev;
	struct event* write_ev;
	char* buffer;
};
 
void release_sock_event(struct sock_ev* ev)
{
	event_del(ev->read_ev);
	free(ev->read_ev);
	free(ev->write_ev);
	free(ev->buffer);
	free(ev);
}
 
void on_write(int sock, short event, void* arg)
{
	char* buffer = (char*)arg;
	//	char *buffer = "hahahehe";
	send(sock, buffer, strlen(buffer), 0);
	free(buffer);
}
 
void on_read(int sock, short event, void* arg)
{
	struct event* write_ev;
	int size;
	struct sock_ev* ev = (struct sock_ev*)arg;
	ev->buffer = (char*)malloc(MEM_SIZE);
	bzero(ev->buffer, MEM_SIZE);
	size = recv(sock, ev->buffer, MEM_SIZE, 0);
	printf("receive data:%s, size:%d\n", ev->buffer, size);
	if (size == 0) {
		release_sock_event(ev);
		close(sock);
		return;
	}
	event_set(ev->write_ev, sock, EV_WRITE, on_write, ev->buffer);
	event_base_set(base, ev->write_ev);
	event_add(ev->write_ev, NULL);
}
 
void on_accept(int sock, short event, void* arg)
{
	struct sockaddr_in cli_addr;
	int newfd, sin_size;
	struct sock_ev* ev = (struct sock_ev*)malloc(sizeof(struct sock_ev));
	ev->read_ev = (struct event*)malloc(sizeof(struct event));
	ev->write_ev = (struct event*)malloc(sizeof(struct event));
	sin_size = sizeof(struct sockaddr_in);
	newfd = accept(sock, (struct sockaddr*)&cli_addr, &sin_size);
 
	printf("get connection from %s\n",inet_ntoa(cli_addr.sin_addr));
	event_set(ev->read_ev, newfd, EV_READ|EV_PERSIST, on_read, ev);
	event_base_set(base, ev->read_ev);
	event_add(ev->read_ev, NULL);
}
 
int main(int argc, char* argv[])
{
	struct sockaddr_in serv_addr;
	int sock;
 
	sock = socket(AF_INET, SOCK_STREAM, 0);
	int reset = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reset, sizeof(int));
	memset(&serv_addr, 0, sizeof(my_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bind(sock, (struct sockaddr*)&serv_addr, sizeof(struct sockaddr));
	listen(sock, BACKLOG);
 
	struct event listen_ev;
	base = event_base_new();
	event_set(&listen_ev, sock, EV_READ|EV_PERSIST, on_accept, NULL);
	event_base_set(base, &listen_ev);
	event_add(&listen_ev, NULL);
	event_base_dispatch(base);
 
	return 0;
 
 
}
```

libevent相关函数：

#### 创建和销毁 event_base

event_base 是首先需要被创建出来的对象。event_base 结构持有了一个 event 集合。如果 event_base 被设置了使用锁，那么它在多个线程中可以安全的访问。但是对 event_base 的循环（下面会马上解释什么是“对 event_base 的循环”）只能在某个线程中执行。如果你希望多个线程进行循环，那么应该做的就是为每一个线程创建一个 event_base。

event_base 存在多个后端可以选择（我们也把 event_base 后端叫做 event_base 的方法）：

1. select
2. poll
3. epoll
4. kqueue
5. devpoll
6. evport
7. win32

在我们创建 event_base 的时候，Libevent 会为我们选择最快的后端。创建 event_base 通过函数 event_base_new() 来完成：
```
// 创建成功返回一个拥有默认设置的 event base
// 创建失败返回 NULL
struct event_base *event_base_new(void);
 
// 查看 event_base 实际上使用到的后端
const char *event_base_get_method(const struct event_base *base);
```

对于大多数程序来说，默认设置已经够用了。

event_base 的释放使用函数：

>void event_base_free(struct event_base *base);

#### 事件循环（event loop）

event_base 会持有一组 event（这是我们前面说到的），换而言之就是说，我们可以向 event_base 中注册 event（具体如何注册本文先不谈）。如果我们向 event_base 中注册了一些 event，那么就可以让 Libevent 开始事件循环了：
```
// 指定一个 event_base 并开始事件循环
// 此函数内部被实现为一个不断进行的循环
// 此函数返回 0 表示成功退出
// 此函数返回 -1 表示存在未处理的错误
int event_base_dispatch(struct event_base *base);
```
默认的事件循环会在以下的情况停止（也就是 event_base_dispatch 会返回）：

```
如果 base 中没有 event，那么事件循环将停止
调用 event_base_loopbreak()，那么事件循环将停止
调用 event_base_loopexit()，那么事件循环将停止
如果出现错误，那么事件循环将停止
```
事件循环会检测是否存在活跃事件（之前已经介绍过活跃事件这一术语：[http://name5566.com/4190.html](http://name5566.com/4190.html)） ，若存在活跃事件，那么调用事件对应的回调函数。

停止事件循环的可以通过移除 event_base 中的 event 来实现。如果你希望在 event_base 中存在 event 的情况下停止事件循环，可以通过以下函数完成：

```
// 这两个函数成功返回 0 失败返回 -1
// 指定在 tv 时间后停止事件循环
// 如果 tv == NULL 那么将无延时的停止事件循环
int event_base_loopexit(struct event_base *base,
const struct timeval *tv);
// 立即停止事件循环（而不是无延时的停止）
int event_base_loopbreak(struct event_base *base);
```

这里需要区别一下 event_base_loopexit(base, NULL) 和 event_base_loopbreak(base)：

1. event_base_loopexit(base, NULL) 如果当前正在为多个活跃事件调用回调函数，那么不会立即退出，而是等到所有的活跃事件的回调函数都执行完成后才退出事件循环

2. event_base_loopbreak(base) 如果当前正在为多个活跃事件调用回调函数，那么当前正在调用的回调函数会被执行，然后马上退出事件循环，而并不处理其他的活跃事件了

有时候，我们需要在事件的回调函数中获取当前的时间，这时候你不需要调用 gettimeofday() 而是使用 event_base_gettimeofday_cached()（你的系统可能实现 gettimeofday() 为一个系统调用，你可以避免系统调用的开销）：

1. // 获取到的时间为开始执行此轮事件回调函数的时间
2. // 成功返回 0 失败返回负数
3. int event_base_gettimeofday_cached(struct event_base *base,
4. struct timeval *tv_out);

如果我们需要（为了调试）获取被注册到 event_base 的所有的 event 和它们的状态，调用 event_base_dump_events() 函数：

```
// f 表示输出内容的目标文件
void event_base_dump_events(struct event_base *base, FILE *f);
```

#### event

现在开始详细的讨论一下 event。在 Libevent 中 event 表示了一组条件，例如：

1. 一个文件描述符可读或者可写
2. 超时
3. 出现一个信号
4. 用户触发了一个事件

event 的相关术语：

1. 一个 event 通过 event_new() 创建出来，那么它是已初始化的，另外 event_assign() 也被用来初始化 event（但是有它特定的用法）
2. 一个 event 被注册到（通过 add 函数添加到）一个 event_base 中，其为 pending 状态
3. 如果 pending event 表示的条件被触发了，那么此 event 会变为活跃的，其为 active 状态，则 event 相关的回调函数被调用
4. event 可以是 persistent（持久的）也可以是非 persistent 的。event 相关的回调函数被调用之后，只有 persistent event 会继续转为 pending 状态。对于非 persistent 的 event 你可以在 event 相关的事件回调函数中调用 event_add() 使得此 event 转为 pending 状态

event 常用 API：

```
// 定义了各种条件
// 超时
#define EV_TIMEOUT 0x01
// event 相关的文件描述符可以读了
#define EV_READ 0x02
// event 相关的文件描述符可以写了
#define EV_WRITE 0x04
// 被用于信号检测（详见下文）
#define EV_SIGNAL 0x08
// 用于指定 event 为 persistent
#define EV_PERSIST 0x10
// 用于指定 event 会被边缘触发（Edge-triggered 可参考 http://name5566.com/3818.html）
#define EV_ET 0x20
 
// event 的回调函数
// 参数 evutil_socket_t --- event 关联的文件描述符
// 参数 short --- 当前发生的条件（也就是上面定义的条件）
// 参数 void* --- 其为 event_new 函数中的 arg 参数
typedef void (*event_callback_fn)(evutil_socket_t, short, void *);
 
// 创建 event
// base --- 使用此 event 的 event_base
// what --- 指定 event 关心的各种条件（也就是上面定义的条件）
// fd --- 文件描述符
// cb --- event 相关的回调函数
// arg --- 用户自定义数据
// 函数执行失败返回 NULL
struct event *event_new(struct event_base *base, evutil_socket_t fd,
short what, event_callback_fn cb,
void *arg);
 
// 释放 event（真正释放内存，对应 event_new 使用）
// 可以用来释放由 event_new 分配的 event
// 若 event 处于 pending 或者 active 状态释放也不会存在问题
void event_free(struct event *event);
 
// 清理 event（并不是真正释放内存）
// 可用于已经初始化的、pending、active 的 event
// 此函数会将 event 转换为非 pending、非 active 状态的
// 函数返回 0 表示成功 -1 表示失败
int event_del(struct event *event);
 
// 用于向 event_base 中注册 event
// tv 用于指定超时时间，为 NULL 表示无超时时间
// 函数返回 0 表示成功 -1 表示失败
int event_add(struct event *ev, const struct timeval *tv);
```

一个范例：
```
#include <event2/event.h>
// event 的回调函数
void cb_func(evutil_socket_t fd, short what, void *arg)
{
	const char *data = arg;
	printf("Got an event on socket %d:%s%s%s%s [%s]",
	(int) fd,
	(what&EV_TIMEOUT) ? " timeout" : "",
	(what&EV_READ) ? " read" : "",
	(what&EV_WRITE) ? " write" : "",
	(what&EV_SIGNAL) ? " signal" : "",
	data);
}
 
void main_loop(evutil_socket_t fd1, evutil_socket_t fd2)
{
	struct event *ev1, *ev2;
	struct timeval five_seconds = {5, 0};
	// 创建 event_base
	struct event_base *base = event_base_new();
	 
	// 这里假定 fd1、fd2 已经被设置好了（它们都被设置为非阻塞的）
	 
	// 创建 event
	ev1 = event_new(base, fd1, EV_TIMEOUT | EV_READ | EV_PERSIST, cb_func,(char*)"Reading event");
	ev2 = event_new(base, fd2, EV_WRITE | EV_PERSIST, cb_func,(char*)"Writing event");
	 
	// 注册 event 到 event_base
	event_add(ev1, &five_seconds);
	event_add(ev2, NULL);
	// 开始事件循环
	event_base_dispatch(base);
}

```
Libevent 能够处理信号。信号 event 相关的函数：
```
// base --- event_base
// signum --- 信号，例如 SIGHUP
// callback --- 信号出现时调用的回调函数
// arg --- 用户自定义数据
#define evsignal_new(base, signum, callback, arg) \
event_new(base, signum, EV_SIGNAL|EV_PERSIST, cb, arg)
 
// 将信号 event 注册到 event_base
#define evsignal_add(ev, tv) \
event_add((ev),(tv))
 
// 清理信号 event
#define evsignal_del(ev) \
event_del(ev)
```
在通常的 POSIX 信号处理函数中，不少函数是不能被调用的（例如，不可重入的函数），但是在 Libevent 中却没有这些限制，因为信号 event 设定的回调函数运行在事件循环中。另外需要注意的是，在同一个进程中 Libevent 只能允许一个 event_base 监听信号。

#### 性能相关问题

Libevent 提供了我们机制来重用 event 用以避免 event 在堆上的频繁分配和释放。相关的接口：
```
// 此函数用于初始化 event（包括可以初始化栈上和静态存储区中的 event）
// event_assign() 和 event_new() 除了 event 参数之外，使用了一样的参数
// event 参数用于指定一个未初始化的且需要初始化的 event
// 函数成功返回 0 失败返回 -1
int event_assign(struct event *event, struct event_base *base,
evutil_socket_t fd, short what,
void (*callback)(evutil_socket_t, short, void *), void *arg);
 
// 类似上面的函数，此函数被信号 event 使用
#define evsignal_assign(event, base, signum, callback, arg) \
event_assign(event, base, signum, EV_SIGNAL|EV_PERSIST, callback, arg)

```
已经初始化或者处于 pending 的 event，首先需要调用 event_del() 后再调用 event_assign()。

这里我们进一步认识一下 event_new()、event_assign()、event_free()、event_del()

event_new() 实际上完成了两件事情：

1. 通过内存分配函数在堆上分配了 event
2. 使用 event_assign() 初始化了此 event

event_free() 实际上完成了两件事情：

1. 调用 event_del() 进行 event 的清理工作
2. 通过内存分配函数在堆上释放此 event