https://github.com/libevent/libevent


<Libevent深入浅出>本书要求有一定的服务并发编程基础，了解select和epoll等多路I/O复用机制。
https://github.com/aceld/libevent


**[详解libevent网络库（一）---框架的搭建](https://blog.csdn.net/lemon_tea666/category_9040090.html)**

[libevent从入门到掌握<一>](https://zhuanlan.zhihu.com/p/87562010)

[Libevent使用例子，从简单到复杂](https://blog.csdn.net/glw0223/article/details/89648449)

http://libevent.net/

[libevent库介绍](https://blog.csdn.net/byxdaz/article/details/88621246)


-----------------

https://github.com/saosir/libevent-comment


[《libevent编程疑难解答》](https://blog.csdn.net/luotuo44/article/details/39547391)

[《libevent源码分析》](https://blog.csdn.net/luotuo44/article/category/2435521)

[❲追根究底❳Libevent内部实现原理初探](https://www.jianshu.com/p/38acaac55e79)

### BOOK

[libevent-book](https://github.com/nmathewson/libevent-book) Nick's libevent manual

[libevent-book](https://github.com/vison0300/libevent-book) libevent-book html 安定


### blog

[libevent在visual studio上实践碰到的问题和解决](https://www.cnblogs.com/csdreamer/archive/2013/05/23/3094072.html)

[翻译：libevent参考手册第九章：连接监听器：接受TCP连接 (十一)](https://www.cnblogs.com/csdreamer/articles/3068155.html)

[翻译：libevent参考手册第八章：evbuffer：缓冲IO实用功能 (十)](https://www.cnblogs.com/csdreamer/articles/3068150.html)

[翻译：libevent参考手册第七章：Bufferevent：高级话题 (九)](https://www.cnblogs.com/csdreamer/articles/3068139.html)

[翻译：libevent参考手册第六章：bufferevent：概念和入门 (八)](https://www.cnblogs.com/csdreamer/articles/3068136.html)

[翻译：libevent参考手册第五章：辅助类型和函数 (七)](https://www.cnblogs.com/csdreamer/articles/3067911.html)

[翻译：libevent参考手册第四章：与事件一起工作 (六)](https://www.cnblogs.com/csdreamer/articles/3067900.html)

[翻译：Libevent参考手册第三章：与事件循环一起工作 (五)](https://www.cnblogs.com/csdreamer/articles/3067854.html)

[翻译：libevent参考手册第二章：创建event_base (四)](https://www.cnblogs.com/csdreamer/articles/3067831.html)

[翻译：Libevent参考手册第一章：设置libevent (三)](https://www.cnblogs.com/csdreamer/articles/3066092.html)

[翻译：Libevent参考手册：前言 (二)](https://www.cnblogs.com/csdreamer/articles/3066089.html)

[翻译：使用Libevent的快速可移植非阻塞网络编程：异步IO简介 (一)](https://www.cnblogs.com/csdreamer/articles/3064036.html)

[Windows 上静态编译 Libevent 2.0.17 并实现一个简单 HTTP 服务器](https://www.cnblogs.com/csdreamer/articles/3064030.html)

---

[Linux C编程之二十二 Linux线程池实现](https://www.cnblogs.com/xuejiale/p/10919824.html)

[Linux C编程之二十一 Linux高并发web服务器开发](https://www.cnblogs.com/xuejiale/p/10917407.html)

[Linux C编程之二十  xml-json](https://www.cnblogs.com/xuejiale/p/10899035.html)

[Linux C编程之十九（2） libevent](https://www.cnblogs.com/xuejiale/p/10872690.html)

[Linux C编程之十九（1） libevent基本概念](https://www.cnblogs.com/xuejiale/p/10872683.html)

[Linux C编程之四 动态库（共享库）的制作](https://www.cnblogs.com/xuejiale/p/10788340.html)

[Linux C编程之五 gdb调试](https://www.cnblogs.com/xuejiale/p/10788379.html)

[Linux C编程之六 makefile项目管理](https://www.cnblogs.com/xuejiale/p/10788404.html)

[Linux C编程之七（2） 系统IO函数](https://www.cnblogs.com/xuejiale/p/10788695.html)

[Linux C编程之八 文件操作相关函数](https://www.cnblogs.com/xuejiale/p/10789629.html)

[Linux C编程之十 进程及进程控制](https://www.cnblogs.com/xuejiale/p/10791617.html)

[Linux C编程之九 目录操作相关函数](https://www.cnblogs.com/xuejiale/p/10790114.html)

[Linux C编程之一 vim编辑器的使用](https://www.cnblogs.com/xuejiale/p/10788265.html)

[Linux C编程之二 gcc编译](https://www.cnblogs.com/xuejiale/p/10788311.html)

[Linux C编程之三 静态库的制作和使用](https://www.cnblogs.com/xuejiale/p/10788320.html)

[Linux C编程之十五 线程同步](https://www.cnblogs.com/xuejiale/p/10822896.html)

[Linux C编程之十六 网络编程基础-socket](https://www.cnblogs.com/xuejiale/p/10844445.html)

[Linux C编程之十七 socket编程](https://www.cnblogs.com/xuejiale/p/10846956.html)

[Linux C编程之十八 高并发服务器](https://www.cnblogs.com/xuejiale/p/10849095.html)

[Linux C编程之十二 信号](https://www.cnblogs.com/xuejiale/p/10804566.html)

[Linux C编程之十三 进程组、守护进程](https://www.cnblogs.com/xuejiale/p/10811746.html)

[Linux C编程之十四 线程、线程控制、线程属性](https://www.cnblogs.com/xuejiale/p/10817045.html)

[Linux C编程之十一 进程间通信](https://www.cnblogs.com/xuejiale/p/10803928.html)

[Linux C编程之七（1）系统IO函数](https://www.cnblogs.com/xuejiale/p/10777650.html)

[网络名词术语解析](https://www.cnblogs.com/xuejiale/p/10854302.html)

[gcc使用及动静态库制作](https://www.cnblogs.com/xuejiale/p/10741311.html)

