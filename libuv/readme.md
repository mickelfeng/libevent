
[libuv入门一](https://blog.csdn.net/u010731020/article/details/80945256)

http://luohaha.github.io/Chinese-uvbook/source/introduction.html

**[Chinese-uvbook](https://github.com/luohaha/Chinese-uvbook/)** 翻译的libuv的中文教程

libuv是一个高性能的，事件驱动的I/O库，并且提供了跨平台（如windows, linux）的API。

libuv的官网：http://docs.libuv.org/ git库 [https://github.com/libuv/libuv](https://github.com/libuv/libuv)

首先是根据官方介绍安装libuv:

1、下载源代码

2、然后进行安装步骤：
```
$ sh autogen.sh
$ ./configure
$ make
$ make check
$ make install
```
安装时使用sudo避免权限问题。安装后的库文件在usr/local/lib下。

安装后记得执行一下 ldconfig命令，让动态链接库为系统所共享。不然后边编译链接的时候会提示找不到文件。

测试安装是否成功：

hello.c
```
#include  <stdio.h>
#include  <stdlib.h>
#include  <uv.h>

int  main()
{
    uv_loop_t *loop = malloc(sizeof(uv_loop_t));
    uv_loop_init(loop);

    printf("Now quitting.n");
    
    uv_run(loop, UV_RUN_DEFAULT);
    uv_loop_close(loop);
    
    free(loop);
    return  0;
}
```
将usr/local/lib/libuv.a 和usr/local/include/uv.h放到工作目录下。执行编译命令

>gcc -g -Wall -o hello hello.c -luv libuv.a

  

两个重要的libuv学习库

[https://github.com/nikhilm/uvbook](https://github.com/nikhilm/uvbook)

翻译的libuv的中文教程

[https://github.com/luohaha/Chinese-uvbook](https://github.com/luohaha/Chinese-uvbook)