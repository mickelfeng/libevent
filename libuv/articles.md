

#### [Libuv学习——文件处理](http://zhuanlan.zhihu.com/p/97789391)

#### [nodejs是如何和libuv以及v8一起合作的？(文末有彩蛋哦)](http://zhuanlan.zhihu.com/p/92305600)

#### [从libuv源码中学习红黑树](http://zhuanlan.zhihu.com/p/91359420)

#### [libuv线程池和主线程通信原理](http://zhuanlan.zhihu.com/p/101042661)

#### [Libuv学习——线程基础](http://zhuanlan.zhihu.com/p/87638134)

#### [Libuv学习——线程基础](http://zhuanlan.zhihu.com/p/86669455)

#### [Libuv学习——线程池](http://zhuanlan.zhihu.com/p/91183396)

#### [Libuv学习——队列](http://zhuanlan.zhihu.com/p/83765851)

#### [兄déi，libuv了解一下](http://zhuanlan.zhihu.com/p/50497450)

#### [在单片机运行 uv](http://zhuanlan.zhihu.com/p/81983372)

#### [libuv初探（一）——搭建本地脚手架](http://zhuanlan.zhihu.com/p/76575148)

#### [libuv 源码阅读之准备篇](http://zhuanlan.zhihu.com/p/83776555)

#### [libuv的定时器原理源码解析](http://zhuanlan.zhihu.com/p/60508184)

#### [libuv编译最佳实践](http://zhuanlan.zhihu.com/p/31344416)

#### [libuv的典型应用——CTP的Node.js封装](http://zhuanlan.zhihu.com/p/46288577)

#### [libuv之线程池以及线程间通信源码解析](http://zhuanlan.zhihu.com/p/60508840)


---

#### [nodejs深入学习系列之libuv基础篇(一)](http://zhuanlan.zhihu.com/p/86242398)

#### [nodejs深入学习系列之libuv基础篇(二)](http://zhuanlan.zhihu.com/p/86249296)

#### [nodejs的http.createServer过程解析](http://zhuanlan.zhihu.com/p/60517110)

#### [nodejs之启动源码浅析](http://zhuanlan.zhihu.com/p/60516991)

#### [Node.js 异步非阻塞 I/O 机制剖析](http://zhuanlan.zhihu.com/p/74119491)

#### [Node.js 事件循环的工作流程 & 生命周期](http://zhuanlan.zhihu.com/p/35918797)

#### [Node.js 异步：文件 I/O](http://zhuanlan.zhihu.com/p/64953289)

#### [NodeJS 事件循环（第四部分）- 处理 IO](http://zhuanlan.zhihu.com/p/37756195)

#### [浅析libuv源码-node事件轮询解析(1)](http://zhuanlan.zhihu.com/p/64855113)

#### [浅析libuv源码-node事件轮询解析(2)](http://zhuanlan.zhihu.com/p/64967630)

#### [浅析libuv源码-node事件轮询解析(3)](http://zhuanlan.zhihu.com/p/65350725)

---

#### [Node中libuv的I/O线程池与PHP之类创建的线程有啥区别？](/question/35675550/answer/230253979)

#### [为什么libuv的uv_idle_init函数只有声明没有定义？](/question/57256811/answer/152177075)

#### [如何评价小鬼王琳凯的星途？](/question/270937332/answer/393296772)

#### [Node中libuv的I/O线程池与PHP之类创建的线程有啥区别？](/question/35675550/answer/233075901)

#### [Node中libuv的I/O线程池与PHP之类创建的线程有啥区别？](/question/35675550/answer/63992587)

#### [nodejs的网络io线程是单线程的吗？](/question/270513721/answer/354629766)

#### [添加 libuv 的 CMakeLists 怎样写？](/question/47308370/answer/147442405)

#### [添加 libuv 的 CMakeLists 怎样写？](/question/47308370/answer/105613721)

#### [nodejs中libuv是多线程的，那在并发情况下读写文件的过程（不是回调函数）会产生冲突吗?](/question/296131096/answer/498826468)

#### [nodejs的网络io线程是单线程的吗？](/question/270513721/answer/845154886)

#### [libuv网络库中的listen io监视器向主循环中重复注册的操作有没有问题？](/question/327722190/answer/707842143)



#### [男生会在乎女生的家境嘛？](/question/42021846/answer/399511472)

#### [如何看待受教育程度和道德高低的关系？](/question/277401106/answer/393286777)

[我在等風也等你](http://www.zhihu.com/people/shuai-ge-60-1-54)