# libevent_study


[tcpdaemon](http://git.oschina.net/calvinwilliams/tcpdaemon/tree/master) 是一个TCP通讯服务端平台/库，

它封装了众多常见服务端进程/线程管理和TCP连接管理模型（Forking、Leader-Follow、IO-Multiplex、WindowsThreads Leader-Follow），使用者只需加入TCP通讯数据收发和应用逻辑代码就能快速构建出完整的TCP应用服务器。

[libevent-1.4.15源码阅读注释](https://github.com/liangsijian/libevent-comment)

[libevlite](https://github.com/spriteray/libevlite) libevlite - A simple Network library that contains the Event library, Thread library, Communications library. Event library is LIBEVENT Lite Edition, so named for the LIBEVLITE

[python-libevent](https://github.com/fancycode/python-libevent)

[libeventInterface](https://github.com/xuwening/libeventInterface) C++实现，对libevent库二次开发，解决libevent和自有服务融合问题

[lev](https://github.com/YasserAsmi/lev)Lightweight C++ wrapper for LibEvent 2 API

[luaevent](https://github.com/harningt/luaevent) libevent binding for Lua




### [libevent、libev、libuv、IOCP、asio、muduo优劣分析、QT下编译libevent静态库](https://blog.csdn.net/tjm1017/article/details/88219576)

看到很多技术牛人写的文章，受益匪浅，也很羡慕，羡慕他们的技术还有文采。平时自己的技术积累都记在自己的笔记上，写成博客分享给别人也是个很不错的选择，

今天分享一个网络库选择的文章，当时我负责公司服务器的改造工作，需要选择一个适合的开源网络库。

欲使用开源网络库libevent、libev、libuv、IOCP、asio、muduo中的一个，经过对比分析，因本服务器系统目前于windows下运行，考虑到未来跨平台的支持，最终选定libevent开源网络库。

技术对比：

1.libevent

C语言跨平台，应用最广泛，历史悠久的跨平台事件库。这是一个用纯C写的开源库，属于一个轻量级的网络中间件。其中用到的基本数据结构也是非常巧妙。展现反应堆模型的基本使用方法。不同的事件对应不容的处理方法。I/O 定时 信号。三种事件的处理单独分开，又通过事件驱动融合在一起。

 优先级特性：激活的事件组织在优先级队列中，各类事件默认的优先级是相同的，可以通过设置事件的优先级使其优先被处理。

 libevent在windows下支持iocp，但还不完善。

2.libev

设计更简练，性能更好，但因对Windows支持不够好，排除。

优先级特性：激活的事件组织在优先级队列中，各类事件默认的优先级是相同的，可以通过设置事件的优先级使其优先被处理。

3.libuv

libuv是一个支持多平台的异步IO库。它主要是为了node.js而开发的。

优先级特性：没有优先级概念，按照固定的顺序访问各类事件。

因libuv为node.js定向开发，普遍应用性不好，且事件触发不可设定优先级，排除。

4.IOCP

IOCP是windows下IO事件处理的最高效的一种方式，结合OVERLAPPED IO可以实现真正的完全异步IO。不支持跨平台。

考虑到未来跨平台的可能，排除。

5.boost::asio

C++语言跨平台。用bind做回调也并不比虚函数好，看上去灵活了，代价却更高了。不光是运行时的内存和时间代价，编译时间也更长。基于ASIO开发应用，要求程序员熟悉函数对象，函数指针，熟悉boost库中的boost::bind，内存管理控制方面。

asio是一个高性能的网络开发库，Windows下使用IOCP，Linux下使用epoll。

考虑到asio的使用，对现有系统的改造难度大，排除。

6.Muduo

这是一个用纯c++写的库，仅在linux下使用，one loop per thread的思想贯穿其中，将I/O 定时 信号都通过文件描述符的方式融合在一起，三类事件等同于一类事件来看待。

因仅在linux下使用，故而排除。

最终选择了lievent网络库作为高并发服务器改造使用库，原服务器在QT上开发的，另附上QT下编译libevent的方法：

QT下编译libevent静态库

1.  libevent官网下载最新版本的源码包。http://libevent.org/
2.  解压源码包后，使用mingw编译器编译libevent。
```
# tar zxvf libevent-1.2.tar.gz  
# cd libevent-1.2  
# ./configure

# make  
# make install
```
将生成的libevent.a、libevent_core.a、libevent_extras.a静态库文件复制到qt的项目文件夹下，添加到qt项目中。
