
[Libevent:Helper function](https://www.jianshu.com/p/2f540e0ccaa3)

## 基本类型

- evutil_socket_t
**大多数操作系统，除了Windows，一个socket就是一个int。而在windows里，一个socket是SOCKET类型。Libevent里定义了一个类型evutil_socket_t**
```
#ifdef WIN32
#define evutil_socket_t intptr_t
#else
#define evutil_socket_t int
#endif
```

## 可移植时间函数

不是所有平台都定义了标准时间操作函数，所以Libevent提供了一系列

    #define evutil_timeradd(tvp, uvp, vvp) 
    #define evutil_timersub(tvp, uvp, vvp) 

这两个宏把头两个参数进行加减操作存储在第三个参数里。

    #define evutil_timerclear(tvp) 
    #define evutil_timerisset(tvp) 

第一个函数清楚时间，第二个检查是否被设置

    #define evutil_timercmp(tvp, uvp, cmp)

    int evutil_gettimeofday(struct timeval *tv,struct timezone *tz);

这个函数把tv设置为当前时间,第二个参数无用
```
struct timeval tv1, tv2, tv3;

/* Set tv1 = 5.5 seconds */
tv1.tv_sec = 5; tv1.tv_usec = 500*1000;

/* Set tv2 = now */
evutil_gettimeofday(&tv2, NULL);

/* Set tv3 = 5.5 seconds in the future */
evutil_timeradd(&tv1, &tv2, &tv3);

/* all 3 should print true */
if (evutil_timercmp(&tv1, &tv1, ==))  /* == "If tv1 == tv1" */
   puts("5.5 sec == 5.5 sec");
if (evutil_timercmp(&tv3, &tv2, >=))  /* == "If tv3 >= tv2" */
   puts("The future is after the present.");
if (evutil_timercmp(&tv1, &tv2, <))   /* == "If tv1 < tv2" */
   puts("It is no longer the past.");

```
## socket兼容性API

```
int evutil_closesocket(evutil_socket_t s);

#define EVUTIL_CLOSESOCKET(s) evutil_closesocket(s)

#define EVUTIL_SOCKET_ERROR()
#define EVUTIL_SET_SOCKET_ERROR(errcode)
#define evutil_socket_geterror(sock)
#define evutil_socket_error_to_string(errcode)
int evutil_make_socket_nonblocking(evutil_socket_t sock);
int evutil_make_listen_socket_reuseable(evutil_socket_t sock);
int evutil_make_socket_closeonexec(evutil_socket_t sock);
int evutil_socketpair(int family, int type, int protocol,
        evutil_socket_t sv[2]);

```
# string移植性函数
```
ev_int64_t evutil_strtoll(const char *s, char **endptr, int base);
int evutil_snprintf(char *buf, size_t buflen, const char *format, ...);
int evutil_vsnprintf(char *buf, size_t buflen, const char *format, va_list ap);
int evutil_ascii_strcasecmp(const char *str1, const char *str2);
int evutil_ascii_strncasecmp(const char *str1, const char *str2, size_t n);
```
# IPv6支持函数
```
const char *evutil_inet_ntop(int af, const void *src, char *dst, size_t len);
int evutil_inet_pton(int af, const char *src, void *dst);
int evutil_parse_sockaddr_port(const char *str, struct sockaddr *out,
    int *outlen);
int evutil_sockaddr_cmp(const struct sockaddr *sa1,
    const struct sockaddr *sa2, int include_port);
```
# 随机数生成器
```
void evutil_secure_rng_get_bytes(void *buf, size_t n);
int evutil_secure_rng_init(void);
void evutil_secure_rng_add_bytes(const char *dat, size_t datlen);
```