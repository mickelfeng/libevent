[libev 介绍](https://www.jianshu.com/p/2c78f7ec7c7f)

libev - 一个 C 编写的功能全面的高性能事件循环。

特性

Libev 支持文件描述符事件的 select，poll，Linux 特有的 epoll，BSD 特有的 kqueue 以及 Solaris 特有的事件端口机制 (ev_io)，Linux 的 inotify 接口 (ev_stat)，Linux eventfd/signalfd（用于更快更干净的线程间唤醒 (ev_async)/信号处理 (ev_signal)），相对定时器 (ev_timer)，定制重新调度逻辑的绝对定时器 (ev_periodic)，同步的信号 (ev_signal)，进程状态变化事件 (ev_child)，以及处理事件循环机制自身的事件观察者 (ev_idle，ev_embed，ev_prepare 和 ev_check 观察者) 和文件观察者 (ev_stat)，甚至是对 fork 事件的有限支持 (ev_fork)。
它还相当块。


https://github.com/enki/libev

**https://github.com/kindy/libev**

http://dist.schmorp.de/libev/

http://dist.schmorp.de/libev/libev-4.27.tar.gz


[Libev 官方文档学习笔记（1）——概述和 ev\_loop](https://segmentfault.com/a/1190000006173864)（本文）  
[Libev 官方文档学习笔记（2）——watcher 基础](https://segmentfault.com/a/1190000006200077)  
[Libev 官方文档学习笔记（3）——常用 watcher 接口](https://segmentfault.com/a/1190000006679929)  
[使用 libev 构建 TCP 响应服务器的简单流程](https://segmentfault.com/a/1190000006691243)

[Libev源码分析](https://blog.csdn.net/gqtcgq/article/category/5811919)

[libev源码解析——总览](https://blog.csdn.net/breaksoftware/article/details/76066676)

[libev库的框架解析](https://blog.csdn.net/mdpmvpmao/article/details/46011639)

[libev源代码分析--总体框架](https://blog.csdn.net/yusiguyuan/article/details/18274313)

[libev的使用——结合Socket编程](https://blog.csdn.net/cxy450019566/article/details/52606512)


#### libev的事件结构

libev中支持多种事件，包括IO事件、定时器事件(绝对定时、相对定时)、信号等等。对于每一种事件，都有结构体与之对应。如ev_io、ev_timer、ev_signal、ev_child等。libev在C中使用结构体布局实现了多态，可以将ev_watcher结构体看做所有ev_TYPE结构体的基类。




总结
总的来看，libev的结构设计还是非常清晰。如果说，主循环ev_run是libev这棵大树的主干，那么功能强大，数量繁多的watcher就是这棵大树的树叶，而循环上下文ev_loop则是连接主干和树叶的树枝，它们的分工与职责是相当明确的。作为大树的主干，ev_run的代码是非常稳定和干净的，基本上不会掺杂外部开发者的逻辑代码进来。作为叶子的watcher，它的定位也非常明确：专注于自己的领域，只解决某一个类型的问题。不同的watcher以及watcher和主循环之间并没有太多的干扰和耦合，这也是libev能如此灵活扩展的原因。而中间的树枝ev_loop的作用也是不言而喻的，正是因为它在中间的调和，前面两位哥们才能活得这么有个性。

看到这里，libev的主体架构已经比较清楚了，但是似乎还没看到与性能相关的关键代码。与主干代码不一样，这些代码更多的是隐藏在实现具体逻辑的地方，也就是watcher之中的。虽然watcher的使用接口都比较相似，但是不同的watcher，底层的数据结构和处理策略还是不一样的。下面一篇文章我们就来探索一下libev中比较常用的几种watcher的设计与实现。


---

[事件库之Libev(五)](https://my.oschina.net/u/917596/blog/177573)

[事件库之Libev(四)](https://my.oschina.net/u/917596/blog/177300)

[事件库之Libev(三)](https://my.oschina.net/u/917596/blog/177030)

[事件库之Libev(二)](https://my.oschina.net/u/917596/blog/176915)

[事件库之Libev（一）](https://my.oschina.net/u/917596/blog/176658)

[事件库之Redis自己的事件模型-ae](https://my.oschina.net/u/917596/blog/161077)
