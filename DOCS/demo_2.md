#项目中动态链接库：libevent.lib  libevent_core.lib 、 libevent_extras.lib

#使用库文件：项目根目录下面    .\include;.\include\event2

#==============================================
error C3872: "0xa0": 此字符不允许在标识符中使用

报错：

error C3872: '0xa0': this character is not allowed in an identifier

或者    error C3872: "0xa0": 此字符不允许在标识符中使用

这是因为直接复制代码的问题。0xa0是十六进制数，换成十进制就是160，表示汉字的开始。

解决办法：在报错的代码行检查两边的空格，用英文输入法的空格替换掉。

#=============================================

 1>httpd.obj : error LNK2001: 无法解析的外部符号 __imp__puts

将puts改成 printf编译通过