#include <stdio.h>  
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "event2/listener.h"
#include "event2/util.h"
#include "event2/event.h"
#include <event2/event-config.h>
#include <event2/event_struct.h>
  
void onTime(int sock,short event,void *arg)  
{  
    printf("Game over!\n");  
  
    struct timeval tv;  
    tv.tv_sec = 1;  
    tv.tv_usec = 0;  
  
    // 事件执行后,默认就被删除,需要重新add,使之重复执行  
    event_add((struct event*)arg,&tv);  
	printf("timeout\n");
}  
  
int main()  
{  
	WSADATA wsa_data;
WSAStartup(0x0201, &wsa_data);

    // 初始化事件  
	struct event_base *base;

	struct event evTime;

	int flags=0;
	
    base= event_base_new();
    // 设置定时器回调函数  


	event_assign(&evTime, base, -1, flags, onTime, (void*) &evTime);




    struct timeval tv;  // 1s后执行  
  evutil_timerclear(&tv);
tv.tv_sec = 2;
tv.tv_usec = 0;
      
        // 添加事件  
	event_add(&evTime,&tv);  
    // 循环派发事件  
    event_base_dispatch(base);  
  
    return 0;  
}  