#include <stdio.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>
#else

#define FD_SETSIZE 1024
#include<ws2tcpip.h>
#endif


#include <event2/event.h>
#include <event2/http.h>
#include <event2/http_struct.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>


void reqHandler(struct evhttp_request *req,void *arg)
{
	struct evbuffer *buf = evbuffer_new();
	
	// 发送响应
	evbuffer_add_printf(buf, "Thanks for the request");
	evhttp_send_reply(req,HTTP_OK,"Client",buf);
	
	evbuffer_free(buf);
	
	return;
}

int main(int argc,char **argv)
{


#ifdef	WIN32
	WSADATA wsa_data;
	WSAStartup(0x0201, &wsa_data);
#endif

	
	short port = 8002;
	const char *addr = "localhost";
	struct evhttp *httpserv = NULL;

	struct event_base *base;
	struct evhttp *http;
	struct evhttp_bound_socket *handle;

	// 初始化事件库
	base = event_base_new();

        // 启动http服务
	//httpserv = evhttp_start(addr,port);

	/* Create a new evhttp object to handle requests. */
	http = evhttp_new(base);
	if (!http) {
		fprintf(stderr, "couldn't create evhttp. Exiting.\n");
		return 1;
	}


	/* The /dump URI will dump all requests to stdout and say 200 ok. */
	evhttp_set_cb(http, "/", reqHandler, NULL);


		/* Now we tell the evhttp what port to listen on */
	handle = evhttp_bind_socket_with_handle(http, "0.0.0.0", port);
	if (!handle) {
		fprintf(stderr, "couldn't bind to port %d. Exiting.\n",
		    (int)port);
		return 1;
	}


	// 设置回调
	//evhttp_set_gencb(httpserv, reqHandler,NULL);
	printf("Server started on port %d\n",port);

	event_base_dispatch(base);

	return 0;
}
