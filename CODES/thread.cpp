#include<stdio.h>
#include<stdlib.h>
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "event2/listener.h"
#include "event2/util.h"
#include "event2/event.h"
#include <io.h> 
#include<event2/thread.h>

void cmd_cb(int fd, short events, void *arg)
{
    char buf[1024];
    printf("in the cmd_cb\n");

    read(fd, buf, sizeof(buf));
}

int main()
{
    evthread_use_windows_threads();

    //使用默认的event_base配置
    struct event_base *base = event_base_new();

#ifdef WIN32

    struct event *cmd_ev = event_new(base, fileno(stdout),
                                     EV_READ | EV_PERSIST, cmd_cb, NULL);
#else

struct event *cmd_ev = event_new(base, STDIN_FILENO,
                                     EV_READ | EV_PERSIST, cmd_cb, NULL);
#endif

    event_add(cmd_ev, NULL); //没有超时

    event_base_dispatch(base);

    return 0;
}