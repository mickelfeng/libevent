
#include <stdio.h>
#include <string.h>

#ifndef WIN32
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#else

#define FD_SETSIZE 1024
#include<ws2tcpip.h>
#endif


#include <event2/event.h>
#include "event2/bufferevent.h"
#include "event2/buffer.h"
#include "event2/listener.h"
#include "event2/util.h"

  
/*
   要实现windows下更高效的IOCP方式的API,IOCP在准备好读写事件时不会通知你的程序去拷贝数据,而时在数据完成从内核态拷贝到用户态时才通知应用程序. libevent 2提供了 bufferevent 接口,支持这种编程范式 
*/
  
struct event_base *base;  
  
// 读事件  
void onRead(int clifd,short ievent,void *arg)  
{  
    int ilen;  
    char buf[1500];  
  
    ilen = recv(clifd,buf,1500,0);  
  
    if(ilen <= 0)  
    {  
        printf("Client close\n");  
          
        struct event *pread = (struct event*)arg;  
        event_del(pread);  
        delete pread;  
  
#ifndef WIN32
		close(clifd);
#else
		closesocket(clifd);
#endif

        return;  
    }  
      
    buf[ilen] = '\0';  
    printf("Accpet: %s\n",buf);  
}  
  
// 连接事件  
void onAccept(int svrfd,short ievent,void *arg)  
{  
    int clifd;  
    struct sockaddr_in cliaddr;  
  
    socklen_t sinsize = sizeof(cliaddr);  
    clifd = accept(svrfd,(struct sockaddr*)&cliaddr,&sinsize);  
  
    struct event *pread = new event;  
    event_set(pread,clifd,EV_READ|EV_PERSIST,onRead,pread);  // 注册读(写)事件  
    event_base_set(base,pread);  
    event_add(pread,NULL);  
}  
  
  
int main()  
{  
    int svrfd;  
    struct sockaddr_in svraddr;  
  
    memset(&svrfd,0,sizeof(svraddr));  
    svraddr.sin_family = AF_INET;  
    svraddr.sin_port = htons(1234);  
    svraddr.sin_addr.s_addr = inet_addr("127.0.0.1");  
  
    svrfd = socket(AF_INET,SOCK_STREAM,0);  
    bind(svrfd,(struct sockaddr*)&svraddr,sizeof(svraddr));  
  
    listen(svrfd,10);  
  
    // 初始化事件库  
    base = event_base_new();  
  
    // 初始化一个连接事件,EV_PRESIST指定重复执行该事件  
    struct event evlisten;  
    event_set(&evlisten,svrfd,EV_READ|EV_PERSIST,onAccept,NULL);  
  
    // 设置为base事件  
    event_base_set(base,&evlisten);  
    // 添加事件  
    event_add(&evlisten,NULL);  
    // 事件循环  
    event_base_dispatch(base);  
  
    return 0;  
  
}  