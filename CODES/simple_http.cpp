#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/http.h>

bool startHttpServer(const char* ip, int port, void (*cb)(struct evhttp_request *, void *), void *arg)
{
    //创建event_base和evhttp
    event_base* base = event_base_new();

    printf("Using Libevent with backend method %s.",event_base_get_method(base));


    evhttp* http_server = evhttp_new(base);
    if (!http_server) {
        return false;
    }
    //绑定到指定地址上
    int ret = evhttp_bind_socket(http_server, ip, port & 0xFFFF);
    if (ret != 0) {
        return false;
    }
    //设置事件处理函数
    evhttp_set_gencb(http_server, cb, arg);
    //启动事件循环，当有http请求的时候会调用指定的回调
    event_base_dispatch(base);
    evhttp_free(http_server);
    return true;
}
void MyHttpServerHandler(struct evhttp_request* req, void* arg)
{
    //创建要使用的buffer对象
    evbuffer* buf = evbuffer_new();
    if (!buf) {
        return;
    }
    //获取请求的URI
    const char* uri = (char*)evhttp_request_get_uri(req);
    //添加对应的HTTP代码
    evbuffer_add_printf(buf,"Hello,world");

	/*
    evbuffer_add_printf(buf,"<head><title>MyHttpServer</title></head>");
    evbuffer_add_printf(buf, "<body>");
    //根据URI显示不同的页面
    if (strcmp(uri, "/") == 0) {
        evbuffer_add_printf(buf,"<p>Welcome to my http server</p>");
    } else if (strcmp(uri, "/about") == 0) {
        evbuffer_add_printf(buf,"<p>I love C++</p>");
    }
    evbuffer_add_printf(buf, "</body>");
    evbuffer_add_printf(buf,"</html>");
	*/
    //回复给客户端
    evhttp_send_reply(req, HTTP_OK, "OK", buf);
    evbuffer_free(buf);
}
int main(int argc, char** argv)
{
    //Windows 平台套接字库的初始化
#ifdef WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
    //启动服务在地址 127.0.0.1:9000 上
    startHttpServer("127.0.0.1", 9000, MyHttpServerHandler, NULL);
#ifdef WIN32
    WSACleanup();
#endif
    return 0;
}
