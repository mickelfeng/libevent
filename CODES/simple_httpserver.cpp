#include <stdio.h>
#include <stdlib.h>
#include <event2/keyvalq_struct.h>
#include <event2/http.h>
#include <event2/event.h>
#include <event2/buffer.h>

#define DEFAULT_PORT 8080

void http_request_cb(struct evhttp_request *req, void *arg);
void print_request(struct evhttp_request *req, void *arg);
void php_request_cb(struct evhttp_request *req, void *arg);

int main(int argc, char **argv) {
    int port = DEFAULT_PORT;
    if (argc == 2) {
        port = atoi(argv[1]);
    }
    
    struct event_base *base = event_base_new();
    
    struct evhttp *http = evhttp_new(base);
    evhttp_set_gencb(http, http_request_cb, NULL);
    evhttp_set_cb(http, "/index.php", php_request_cb, NULL);
    evhttp_bind_socket(http, "0.0.0.0", port);
    
    event_base_dispatch(base);
    
    return 0;
}

void php_request_cb(struct evhttp_request *req, void *arg) {
    printf("run php_request_cb...\n");
}

void http_request_cb(struct evhttp_request *req, void *arg) {
    printf("run http_request_cb...\n");

    print_request(req, arg);
    
    const char *uri = evhttp_request_get_uri(req);
    printf("uri: %s\n", uri);
    const char *decoded_uri = evhttp_decode_uri(uri);
    printf("decoded_uri: %s\n", decoded_uri);
    
    struct evhttp_uri *decoded = NULL;
    const char *path;
    
    /* Decode the URI */
    decoded = evhttp_uri_parse(uri);
    if (!decoded) {
        printf("It's not a good URI. Sending BADREQUEST\n");
        evhttp_send_error(req, HTTP_BADREQUEST, 0);
        return;
    }
    
    /* Let's see what path the user asked for. */
    path = evhttp_uri_get_path(decoded);
    if (!path) path = "/";
    printf("path: %s\n", path);
    
    struct evbuffer *evb = evbuffer_new();
    
    evbuffer_add_printf(evb, "Server response");
    evhttp_send_reply(req, HTTP_OK, "OK", evb);
    
    evbuffer_free(evb);
}

void print_request(struct evhttp_request *req, void *arg) {
    const char *cmdtype;
    struct evkeyvalq *headers;
    struct evkeyval *header;
    struct evbuffer *buf;
    
    switch (evhttp_request_get_command(req)) {
        case EVHTTP_REQ_GET: cmdtype = "GET"; break;
        case EVHTTP_REQ_POST: cmdtype = "POST"; break;
        case EVHTTP_REQ_HEAD: cmdtype = "HEAD"; break;
        case EVHTTP_REQ_PUT: cmdtype = "PUT"; break;
        case EVHTTP_REQ_DELETE: cmdtype = "DELETE"; break;
        case EVHTTP_REQ_OPTIONS: cmdtype = "OPTIONS"; break;
        case EVHTTP_REQ_TRACE: cmdtype = "TRACE"; break;
        case EVHTTP_REQ_CONNECT: cmdtype = "CONNECT"; break;
        case EVHTTP_REQ_PATCH: cmdtype = "PATCH"; break;
        default: cmdtype = "unknown"; break;
    }
    
    printf("Received a %s request for %s\nHeaders:\n",
           cmdtype, evhttp_request_get_uri(req));
    
    headers = evhttp_request_get_input_headers(req);
    for (header = headers->tqh_first; header;
         header = header->next.tqe_next) {
        printf("  %s: %s\n", header->key, header->value);
    }
    
    buf = evhttp_request_get_input_buffer(req);
    puts("Input data: <<<");
    while (evbuffer_get_length(buf)) {
        int n;
        char cbuf[128];
        n = evbuffer_remove(buf, cbuf, sizeof(cbuf));
        if (n > 0)
            (void) fwrite(cbuf, 1, n, stdout);
    }
    puts(">>>");
}